<?php
/*
	This PHP script is licensed under the GPL

	Author: Terence Yim
	E-mail: chtyim@gmail.com
	Homepage: http://web-news.sourceforge.net
*/
//récupère les données d'un article

	if (!is_requested("art_group") || (strcmp(get_request("art_group"), $_SESSION["newsgroup"]) == 0)) 
		$reply = true;
	else
		$reply = false;

	if (isset($_SESSION["search_txt"])) 
		$search = true;
	else
		$search = false;
		
	

				
//	$nntp = new NNTP($nntp_server, $user, $pass);
	
	if (!$nntp->connect()) {
		echo "<b>".$messages_ini["error"]["nntp_fail"]."</b><br>";
		echo $nntp->get_error_message()."<br>";
	} else {
		if (is_requested("art_group")) {
			$group_info = $nntp->join_group(get_request("art_group"));
		} else {
			$group_info = $nntp->join_group($_SESSION["newsgroup"]);
		}
		
		if ($group_info == NULL) {
			echo "<b>".$messages_ini["error"]["group_fail"].$_SESSION["newsgroup"]." </b><br>";
			echo $nntp->get_error_message()."<br>";
		} else {
			$MIME_Message = $nntp->get_article($article_id);

			if ($MIME_Message == NULL) {
				echo "<b>".$messages_ini["error"]["article_fail"]."$article_id </b><br>";
				echo $nntp->get_error_message()."<br>";
			} else {
			    $message_node = NULL;

			    if ($thread_search_size > 0) {
    			    $header = $MIME_Message->get_main_header();
	    		    if (!isset($header["references"]) || (strlen($header["references"]) == 0)) {
			            $ref = $header["message-id"];
			        } else {
			            $ref = preg_split("/\s+/", trim($header["references"]));
			            $ref = $ref[0];
			        }
			    
                    // Search through +/- n messages only
			        $message_node = $nntp->get_message_thread($article_id - $thread_search_size, $article_id + $thread_search_size, $ref);
			        $message_node->set_show_all_children(TRUE);
			        $message_node->compact_tree();
			    }

				include(BASE."modeles/article_template.php");				
			}
		}	
	}
?>

</font>
