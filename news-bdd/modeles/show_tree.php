<?php

	if (is_requested("expand")) {
		$_SESSION["expand_all"] = TRUE;
		$need_expand = TRUE;
	} elseif (is_requested("collapse")) {
		$_SESSION["expand_all"] = FALSE;
		$need_expand = TRUE;
	} elseif ($renew) {
		$need_expand = TRUE;
		if (!isset($_SESSION["expand_all"])) {
			$_SESSION["expand_all"] = $default_expanded;
		}
	}

	if ($need_expand) {
		$root_node->set_show_all_children($_SESSION["expand_all"]);
		$root_node->set_show_children(TRUE);
	}

	$display_counter = 0;
	if (isset($_SESSION["search_txt"]) && (strcasecmp($message_per_page, "all") != 0)) {
		$nodes = array_slice($root_node->get_children(), ($page - 1)*$message_per_page, $message_per_page);
		echo display_tree($nodes, 0);
	} else {
		echo display_tree($root_node->get_children(), 0);
	}
	
	
	
	
	
	
	// Need to setup the following entries in $config array
	function display_tree($nodes, $level, $indent = "", $expandable = TRUE, $current_aid = FALSE) {
		global $twig;
		
		$count = 0;
		
		foreach ($nodes as $node) {
		
			$message_info = $node->get_message_info();
			
			
			$display_counter++;

					

			if ($node->is_show_children() && ($node->count_children() != 0)) {
				$enfants = display_tree($node->get_children(), $level + 1, $indent, $expandable, $current_aid);
			}
			$count++;
			
			
			
			/////////////////////////:
			// affichage
			
				
			$replace = array (
				'enfants'		=> $enfants,
				'sujet'			=> $message_info->subject,
				'auteur'		=> $message_info->from["name"],
				'mail'			=> $message_info->from["email"],
				'date'			=> datetostr($message_info->date),
				'nntpid'		=> intval($message_info->nntp_message_id),
				'groupe'		=> urlencode($_SESSION["newsgroup"]),
				'level'			=> $level,
				'img_dir'		=> TEMPLATE_DIR."/img/",

				);
				
			$p .= $twig->render('news/panel_tree.html', $replace);

		}
		
		return $p;
	}
	
	
	
?>