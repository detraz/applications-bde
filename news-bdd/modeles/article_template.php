<?php
/*
	This PHP script is licensed under the GPL
	
	Author: Terence Yim
	E-mail: chtyim@gmail.com
	Homepage: http://web-news.sourceforge.net
*/

	$header = $MIME_Message->get_main_header();
	$parts = $MIME_Message->get_all_parts();
	
	if (is_requested("art_group")) {
		$group = get_request("art_group");
	} else {
		$group = $_SESSION["newsgroup"];
	}

	if (sizeof($parts) > 1) {	// We've got attachment
		$attach_file = "";
		for ($i = 1;$i < sizeof($parts);$i++) {
			if (($i != 1) && (($i - 1) % 5 == 0)) {
				$attach_file .= "<br>\r\n";
			}
			if (strcmp($parts[$i]["filename"], "") != 0) {
				$attach_file .= "<a href=\"newsgroups.php?art_group=".urlencode($group)."&message_id=".$article_id."&attachment_id=".$i."\" target=\"_blank\">".$parts[$i]["filename"]."</a>,&nbsp;";
			} else {
				$attach_file .= "<a href=\"newsgroups.php?art_group=".urlencode($group)."&message_id=".$article_id."&attachment_id=".$i."\" target=\"_blank\">".$messages_ini["text"]["no_name"]." $i</a>,&nbsp;";
			}
		}
		if (strlen($attach_file) > 0) {
			$attach_file = substr($attach_file, 0, strlen($attach_file) - 7);
		}
		
	}

	$count = 0;
	
	foreach ($parts as $part) {
		if (stristr($part["header"]["content-type"], "text/html") ) {	// HTML
			$body = filter_html(decode_message_content($part));

			// Replace the image link for internal resources
			$content_map = $MIME_Message->get_content_map();
			$search_array = array();
			$replace_array = array();
			foreach ($content_map as $cid => $aid) {
				$cid = substr($cid, 1, strlen($cid) - 2);
				$search_array[] = "cid:".$cid;
				$replace_array[] = "?art_group=".urlencode($group)."&message_id=".$article_id."&attachment_id=".$aid;
			}
	
			$body = str_replace($search_array, $replace_array, $body);
			
		} elseif (stristr($part["header"]["content-type"], "text")) {	// Treat all other form of text as plain text
			$body = decode_message_content($part);
			$body = msg_format($body);
		} elseif (preg_match("/^image\/(gif|jpeg|pjpeg)/i", $part["header"]["content-type"])) {
			$body = "<img src=\"?art_group=".urlencode($group)."&message_id=$article_id&attachment_id=$count\" border=\"0\">";

		}
		$count++;

	}

	if(is_requested("post") || $_SESSION["auth"])
		$mail = $header["from"]["email"];
		
	$replace = array(
		"mail" 			=> $mail,
		"date"			=> datetostr($header["date"]),
		"auteur" 		=> $header["from"]["name"],
		"sujet"			=> $header["subject"],
		"newsgroup"		=> $group,
		"article"		=> $attach_file.$body,
		"lang"			=> $messages_ini,
		'id'			=> $article_id,
		'search'		=> $search,
		'reply'			=> $reply
			);
			
	echo $twig->render("news/panel_art.html", $replace);

			
			
    if ($message_node) {
		$root_node = $message_node;
		include(BASE."modeles/show_tree.php");
    }
?>