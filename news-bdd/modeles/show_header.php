<?php
/*

	Auteur : Pierre-Louis Guhur
	E-mail: pierre-louis.guhur@laposte.net
*/
//la page d'accueil des news

	$sort_by_list = array("subject", "from", "date");
	if (is_requested("sign")) {
		$sign = get_request("sign");
	}
	if (is_requested("sort")) {
		$sort = get_request("sort");
	}
	if ($renew || $change_mpp) {
		$page = 1;
	} else if (is_requested("page")) {
		$page = intval(get_request("page"));
		if (isset($_SESSION["search_txt"])) {
		    $renew = 0;
		} else {
		    $renew = 1;
		}
	} else if (isset($_SESSION["last_page"])) {
		$page = $_SESSION["last_page"];
	} else {
		$page = 1;
	}

	if (is_requested("search")) {
		unset($_SESSION["search_txt"]);
		$do_search = TRUE;
		$page = 1;
	}

/*
	if (is_requested("sch_option")) {
		$_SESSION["sch_option"] = !$_SESSION["sch_option"];
	}
*/
	if (is_requested("option")) {
		$_SESSION["more_option"] = !$_SESSION["more_option"];
	}

	if (isset($_COOKIE["wn_pref_mpp"])) {
		$message_per_page = $_COOKIE["wn_pref_mpp"];
	}

	$_SESSION["last_page"] = $page;

	if (!$nntp->connect()) {
		$_SESSION["result"] = null;
		echo "<b>".$messages_ini["error"]["nntp_fail"]."</b><br>";
		echo $nntp->get_error_message()."<br>";
		exit;
	} else {
		$group_info = $nntp->join_group($_SESSION["newsgroup"]);

		if ($group_info == NULL) {
			$_SESSION["result"] = null;
			echo "<b>".$messages_ini["error"]["group_fail"].$_SESSION["newsgroup"]." </b><br>";
			echo $nntp->get_error_message()."<br>";
			exit;
		} else {			
			if ($renew || $do_search || ($_SESSION["result"] == null)) {
				$renew = 1;
				$_SESSION["result"] = null;
				if ($group_info["count"] > 0) {
					$_SESSION["article_list"] = $nntp->get_article_list($_SESSION["newsgroup"]);
					if ($_SESSION["article_list"] === FALSE) {
						unset($_SESSION["article_list"]);
						echo "<b>".$messages_ini["error"]["group_fail"].$_SESSION["newsgroup"]." </b><br>";
						echo $nntp->get_error_message()."<br>";
						exit;
					}				
					
					if ($do_search) {
						$search_txt = get_request("search_txt");
						if (get_magic_quotes_gpc()) {
							$search_txt = stripslashes($search_txt);
						} 
						$search_pat = make_search_pattern($search_txt);
						$flat_tree = TRUE;
						$_SESSION["search_txt"] = htmlescape($search_txt);
					} else {
						$search_pat = "//";
						$flat_tree = FALSE;
						unset($_SESSION["search_txt"]);
					}					
					
					if ((strcmp($message_per_page, "all") == 0) || $do_search) {
						// Search through all messages
						$start_id = 0;
						$end_id = sizeof($_SESSION["article_list"]) - 1;
					} else {
						$end_id = sizeof($_SESSION["article_list"]) - $message_per_page*($page - 1) - 1;
						$start_id = $end_id - $message_per_page + 1;
					}
					if ($start_id < 0) {
						$start_id = 0;
					}

					$result = $nntp->get_message_summary($_SESSION["article_list"][$start_id], $_SESSION["article_list"][$end_id], $search_pat, $flat_tree);
					if ($result) {
						$result[0]->compact_tree();						
						$need_sort = TRUE;
						krsort($result[1], SORT_NUMERIC);
						reset($result[1]);
					}
		
					// Set the tree sorting setting as previous group and force sorting
					if (!isset($sort) && isset($_SESSION["sort_by"]) && $need_sort) {
						$sort = $_SESSION["sort_by"];
						$_SESSION["sort_by"] = -1;
					}
				
					$_SESSION["result"] = $result;
				} else {
					$_SESSION["article_list"] = array();
					$_SESSION["result"] = array(new MessageTreeNode(NULL), array());
				}
			}
		}
		// Quit sooner to release resources
		$nntp->quit();
	}

// Control panel display section
	if ($_SESSION["result"]) {
		$root_node =& $_SESSION["result"][0];
		$ref_list =& $_SESSION["result"][1];
		
		if (!isset($_SESSION["sort_by"])) {
			$_SESSION["sort_by"] = 2;
			$last_sort = -1;
			$_SESSION["sort_asc"] = 0;
			$last_sort_dir = 0;
		} else {
			$last_sort = $_SESSION["sort_by"];
			$last_sort_dir = $_SESSION["sort_asc"];
			if (isset($sort)) {				
				$_SESSION["sort_by"] = intval($sort);
				if ($_SESSION["sort_by"] == $last_sort) {
					$_SESSION["sort_asc"] = ($_SESSION["sort_asc"] == 1)?0:1;
				}
			} else {
				$_SESSION["sort_by"] = $last_sort;
			}
		}
			
		if (($_SESSION["sort_by"] != $last_sort) || ($_SESSION["sort_asc"] != $last_sort_dir)){
			$root_node->deep_sort_message($sort_by_list[$_SESSION["sort_by"]], $_SESSION["sort_asc"]);
		}
		
		if (isset($sign) && isset($mid)) {
			$message_id = $ref_list[$mid][0];
			$references = $ref_list[$mid][1];
			$node =& $root_node;
			
			// Search the reference list only when the expand node is not a child of the root
			if (!$node->get_child($message_id)) {	
				if (sizeof($references) != 0) {
					foreach ($references as $ref) {
						$child =& $node->get_child($ref);
						if ($child != NULL) {
							$node =& $child;
						}
					}
				}
			}

			$node =& $node->get_child($message_id);

			if ($node) {
				if (strcasecmp($sign, "minus") == 0) {
					$node->set_show_children(FALSE);
				} else if (strcasecmp($sign, "plus") == 0) {
					$node->set_show_all_children(TRUE);
				}
			}
		}
		
		if (isset($_SESSION["search_txt"])) {
			if (sizeof($root_node->get_children()) == 0) {
				$info_msg["msg"] = $messages_ini["text"]["sch_notfound"]." - ".$_SESSION["search_txt"];
			} else {
				$info_msg["msg"] = $messages_ini["text"]["sch_found1"]." ".sizeof($root_node->get_children())." ".$messages_ini["text"]["sch_found2"]." - ".$_SESSION["search_txt"].".";
			}
		}
		if ($_SESSION["sort_asc"]) {
			$arrow_img = BASE.$image_base."sort_arrow_up.gif";
		} else {
			$arrow_img = BASE.$image_base."sort_arrow_down.gif";
		}

	}

// Pagination number generation

	if (strcasecmp($message_per_page, "all") != 0) {
		if (isset($_SESSION["search_txt"])) {		// Count from the number of search results
			$page_count = ceil((float)sizeof($root_node->get_children())/(float)$message_per_page);
		} else {
			$page_count = ceil((float)sizeof($_SESSION["article_list"])/(float)$message_per_page);
		}
		$start_page = (ceil($page/$pages_per_page) - 1)*$pages_per_page + 1;
		$end_page = $start_page + $pages_per_page - 1;
		if ($end_page > $page_count) {
			$end_page = $page_count;
		}
	} else {	// Show All
		$page_count = 0;
	}
	if (($page_count != 0) && (($start_page != 1) || ($start_page != $end_page))) {
		$pagination = true;
	} else {
		$pagination = false;
	}

	
	
	
	$search = isset($_SESSION["search_txt"])?$_SESSION["search_txt"]:$messages_ini["control"]["search"]; 
	
	////////////////
	//affichage
	
	$replace = array (
		'infomsg' 	=> $info_msg["msg"],
		'search'	=> $search,
		'lang'		=> $messages_ini,
		'arrow_img' => $arrow_img,
		'pagination'=> $pagination,
		'start_page'=> $start_page,
		'page'		=> $page,
		'end_page'	=> $end_page,
		'page_count'=> $page_count,
		'sort_by'	=> intval($_SESSION["sort_by"])
		);
		
	echo $twig->render('news/panel_head.html', $replace);

	
	////////////////
	//arbre
	
	include(BASE."modeles/show_tree.php");
?>