<?php

header("Content-Type: text/plain"); // Utilisation d'un header pour sp�cifier le type de contenu de la page. Ici, il s'agit juste de texte brut (text/plain). 

	if(!defined("ROOT")) {
		define("ROOT", "../../");
		define("BASE", ROOT."news/");
		define("TEMPLATES", BASE."templates/");
	}

$article_id = (isset($_GET["article_id"])) ? intval($_GET["article_id"]) : NULL;
$groupe = (isset($_GET["art_group"])) ? ($_GET["art_group"]) : NULL;

if ($article_id && $groupe) {
	$stop = true;

	require(BASE."newsgroups.php");//chargement des en-t�tes et configuration


	if (!$nntp->connect()) {
		echo "<b>".$messages_ini["error"]["nntp_fail"]."</b><br>";
		echo $nntp->get_error_message()."<br>";
	} else {
		
		$group_info = $nntp->join_group($groupe);
		if ($group_info == NULL) {
			echo "<b>".$messages_ini["error"]["group_fail"].$_SESSION["newsgroup"]." </b><br>";
			echo $nntp->get_error_message()."<br>";
		} else {
			
			$MIME_Message = $nntp->get_article($article_id);
			if ($MIME_Message == NULL) {
				echo "<b>".$messages_ini["error"]["article_fail"]."$article_id </b><br>";
				echo $nntp->get_error_message()."<br>";
			} else {
				
			    $parts = $MIME_Message->get_all_parts();
				$header = $MIME_Message->get_main_header();
							
				if (sizeof($parts) > 1) {	// We've got attachment
					echo $messages_ini["text"]["attachments"]."\r\n";
					$attach_file = "";
					for ($i = 1;$i < sizeof($parts);$i++) {
						if (($i != 1) && (($i - 1) % 5 == 0)) {
							$attach_file .= "<br>\r\n";
						}
						if (strcmp($parts[$i]["filename"], "") != 0) {
							$attach_file .= "<a href=\"?art_group=".urlencode($group)."&message_id=".$article_id."&attachment_id=".$i."\" target=\"_blank\">".$parts[$i]["filename"]."</a>,&nbsp;";
						} else {
							$attach_file .= "<a href=\"?art_group=".urlencode($group)."&message_id=".$article_id."&attachment_id=".$i."\" target=\"_blank\">".$messages_ini["text"]["no_name"]." $i</a>,&nbsp;";
						}
					}
					if (strlen($attach_file) > 0) {
						$attach_file = substr($attach_file, 0, strlen($attach_file) - 7);
					}
					echo $attach_file;
				}

				$count = 0;
			
				foreach ($parts as $part) {
					
					if (stristr($part["header"]["content-type"], "text/html")) {	// HTML
					
						$body = filter_html(decode_message_content($part));

						// Replace the image link for internal resources
						$content_map = $MIME_Message->get_content_map();
						$search_array = array();
						$replace_array = array();
						foreach ($content_map as $cid => $aid) {
							$cid = substr($cid, 1, strlen($cid) - 2);
							$search_array[] = "cid:".$cid;
							$replace_array[] = "?art_group=".urlencode($group)."&message_id=".$article_id."&attachment_id=".$aid;
						}
				
						$body = str_replace($search_array, $replace_array, $body);
						
						echo $body;
					} elseif (stristr($part["header"]["content-type"], "text")) {	// Treat all other form of text as plain text
						$body = decode_message_content($part);
						$body = msg_format($body);
						echo $body;
					} elseif (preg_match("/^image\/(gif|jpeg|pjpeg)/i", $part["header"]["content-type"])) {
						echo "<img src=\"?art_group=".urlencode($group)."&message_id=$article_id&attachment_id=$count\">";
					}
					$count++;
				}	

				/*
				$message_node = NULL;
				
			    if ($thread_search_size > 0) {
    			    $header = $MIME_Message->get_main_header();
	    		    if (!isset($header["references"]) || (strlen($header["references"]) == 0)) {
			            $ref = $header["message-id"];
			        } else {
			            $ref = preg_split("/\s+/", trim($header["references"]));
			            $ref = $ref[0];
			        }
			    
                    // Search through +/- n messages only
			        $message_node = $nntp->get_message_thread($article_id - $thread_search_size, $article_id + $thread_search_size, $ref);
			        $message_node->set_show_all_children(TRUE);
			        $message_node->compact_tree();
			    }
				*/
								
			}
		}	
	}
	
} 



?>