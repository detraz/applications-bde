<div class="nav-row" >
	<ul class="nav nav-pills">
 
		<li class="dropdown col-xs-3 {IS_DAY}">
			<a class="dropdown-toggle" title="{L_DAY}" href="day.php?cal={CAL}&amp;getdate={GETDATE}">
				<img src="templates/{TEMPLATE}/images/day_on.gif" class="img-thumbnail" alt="{L_DAY}" title="{L_DAY}" border="0" />
			</a>
		</li>
		<li class="dropdown col-xs-3 {IS_WEEK}">
			<a title="{L_WEEK}" class="dropdown-toggle"  href="week.php?cal={CAL}&amp;getdate={GETDATE}">
				<img src="templates/{TEMPLATE}/images/week_on.gif" alt="{L_WEEK}" class="img-thumbnail"  title="{L_WEEK}" border="0" />
			</a>
		</li>
		<li class="dropdown col-xs-3 {IS_MONTH}">
			<a title="{L_MONTH}" class="dropdown-toggle"  href="month.php?cal={CAL}&amp;getdate={GETDATE}">
				<img src="templates/{TEMPLATE}/images/month_on.gif" alt="{L_MONTH}" class="img-thumbnail"  title="{L_MONTH}" border="0" />
			</a>
		</li>
		<li class="dropdown col-xs-3 {IS_YEAR}">
			<a title="{L_YEAR}" class="dropdown-toggle"  href="year.php?cal={CAL}&amp;getdate={GETDATE}">
				<img src="templates/{TEMPLATE}/images/year_on.gif" alt="{L_YEAR}" class="img-thumbnail"  title="{L_YEAR}" border="0" />
			</a>
		</li>
	</ul>	
	
</div>