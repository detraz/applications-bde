<?php
if(!defined("ROOT"))	define("ROOT", "../../");

require_once(ROOT. "lib/iCalcreator.class.php" );
require_once(ROOT."config.php");

/* chargement du fichier */
$config = array( "unique_id" => "bde.crans.org"
               , "directory" => "../calendars/recur_tests"
               , "filename"  => "bde.ics" );
 
$v = new vcalendar( $config );
$v->parse(); 

/* modification d'un évent */
if(isset($_POST['edit_event'])) {
	$uid = stripslashes($_GET['uid']);
	$selectSpec = array( "UID" => $uid );
	$specComps = $v->selectComponents( $selectSpec );
	$test = false;
	foreach( $specComps as $vevent ) {
		$sum = strip_tags($_POST['titre']);
		$descript = strip_tags($_POST['descript']);
		$lieu = strip_tags($_POST['lieu']);
		$start = array('year' 	=> filter_var($_POST['d_annee'], FILTER_SANITIZE_NUMBER_INT),
					'month'		=> filter_var($_POST['d_mois'], FILTER_SANITIZE_NUMBER_INT), 
					'day'		=> filter_var($_POST['d_jour'], FILTER_SANITIZE_NUMBER_INT),
					'hour'		=> filter_var($_POST['d_hour'], FILTER_SANITIZE_NUMBER_INT),
					'min'=> 0, 'sec'=> 0);
		$end = array('year' 	=> filter_var($_POST['f_annee'], FILTER_SANITIZE_NUMBER_INT),
					'month'		=> filter_var($_POST['f_mois'], FILTER_SANITIZE_NUMBER_INT), 
					'day'		=> filter_var($_POST['f_jour'], FILTER_SANITIZE_NUMBER_INT),
					'hour'		=> filter_var($_POST['f_hour'], FILTER_SANITIZE_NUMBER_INT),
					'min'=> 0, 'sec'=> 0);
		$vevent->setProperty( 'dtstart', $start );
		$vevent->setProperty( 'dtend', $end );
		$vevent->setProperty( 'LOCATION', $lieu);
		$vevent->setProperty( 'summary', $sum);
		$vevent->setProperty( 'description', $descript);
		$v->setComponent ( $vevent, $uid );
		$test = true;
	}
	if(!$test) {
		echo $uid;
		$error = "ics_fake_id";
	}
	elseif( !$v->saveCalendar() ) {
		$error = "ics_save";
	}
	else {
		$success = "ics_save";
	}
}
/* création d'un event */
if(isset($_POST['add_event'])) {
	$sum = strip_tags($_POST['titre']);
	$descript = strip_tags($_POST['descript']);
	
	if(empty($sum)&&empty($descript)) {
		$error = "fake_donnee";
	}
	else {
		$vevent = & $v->newComponent( "vevent" );	
		$start = array('year' 	=> filter_var($_POST['d_annee'], FILTER_SANITIZE_NUMBER_INT),
					'month'		=> filter_var($_POST['d_mois'], FILTER_SANITIZE_NUMBER_INT), 
					'day'		=> filter_var($_POST['d_jour'], FILTER_SANITIZE_NUMBER_INT),
					'hour'		=> filter_var($_POST['d_hour'], FILTER_SANITIZE_NUMBER_INT),
					'min'=> 0, 'sec'=> 0);
		$end = array('year' 	=> filter_var($_POST['f_annee'], FILTER_SANITIZE_NUMBER_INT),
					'month'		=> filter_var($_POST['f_mois'], FILTER_SANITIZE_NUMBER_INT), 
					'day'		=> filter_var($_POST['f_jour'], FILTER_SANITIZE_NUMBER_INT),
					'hour'		=> filter_var($_POST['f_hour'], FILTER_SANITIZE_NUMBER_INT),
					'min'=> 0, 'sec'=> 0);
		$vevent->setProperty( 'dtstart', $start );
		$vevent->setProperty( 'dtend', $end );
		$vevent->setProperty( 'LOCATION', strip_tags($_POST['lieu']));
		$vevent->setProperty( 'summary', $sum);
		$vevent->setProperty( 'description', $descript);

		if( !$v->saveCalendar() ) {
			$error = "ics_save";
		}
		else {
			$success = "ics_save";
		}
	}
	
}
/* Récupération des infos de l'event parcouru */
elseif(isset($_GET['uid'])) {
	$uid = stripslashes($_GET['uid']);
	$selectSpec = array( "UID" => $uid );
	$specComps = $v->selectComponents( $selectSpec );
	$test = false;
	foreach( $specComps as $vevent ) {

		$start = $vevent->getProperty( 'dtstart' );
		$end = $vevent->getProperty( 'dtend');
		$lieu = $vevent->getProperty( 'LOCATION');
		$sum = $vevent->getProperty( 'summary');
		$descript = $vevent->getProperty( 'description' );
		$test = true;
	}
	if(!$test) {
		$error = "ics_fake_id";
	}
}

/* Démarrer le template */
include(ROOT."templates.php");
include(ROOT."lib/success.php");
include(ROOT."lib/error.php");

$replace = array (
	'uid'			=> $uid,
	'lieu'  		=> $lieu,
	'start'			=> $start,
	'end'			=> $end,
	'titre'			=> $sum,
	'descript'		=> $descript,
	'l'				=> $langue->out()
				);
echo $twig->render("agenda/adm.html", $replace);
?>