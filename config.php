<?php
//configuration du serveur
	set_time_limit(120);
	header('Content-Type: text/html; charset=utf-8');
	
//récupération des classes et fonctions utiles
require_once(ROOT."lib/utile.php");
require_once(ROOT."lib/log.php");	

// Start the session before output anything
	session_name();
	LogoutRequest();
	session_start();
	
	
//configuration des constantes/var
	$nom_app = "BDE ENS Cachan";
	$head_app = "templates/head.app.html";
	$menu_app = "templates/menu.app.html";
/*
	$dbms = 'mysql';
	$dbhost = 'localhost';
	$dbport = '';
	$dbname = 'club-bde2014';
	$dbuser = 'club-bde2014';
	$dbpasswd = 'e9QhPwvjsZvNEUpd';*/
	$dbhost="localhost";
	$dbname="app";
	$dbuser="root";
	$dbpasswd="";
	$table_prefix = '';

	$langue_dir = ROOT."lib/language/";
	$langue_suf = "fr";//suffixe des fichiers 
	$langue = new Langue($langue_dir);

	$CAS=array(
		'crans.org' => array( 
			'nom'		=> 'Cr@ns',
			'host' 		=> 'cas.crans.org', 
			'context' 	=> '/cas', 
			'port' 		=> 443, 
			'root_cert' => '/etc/ssl/certs/cacert.org.pem'
		),
		'ens-cachan.fr' => array( 
			'nom'		=> 'ENS de Cachan',
			'host' 		=> 'cas.ens-cachan.fr', 
			'context' 	=> '', 
			'port' 		=> 443, 
			'root_cert' => '/etc/ssl/certs/UTN_USERFirst_Hardware_Root_CA.pem'
		),
	);

	
//connexion à la BDD
	dbconn();
	
//gérer la connexion
if(!is_loged() && !$membre)
{
	require_once(ROOT."membre/connect.php");
	exit;
}

?>