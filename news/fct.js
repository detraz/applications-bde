function envoyer(valeur)
{
	document.forms["regle"].group.value = valeur
	document.forms["regle"].submit()
}



function cacher(id)
{
	document.getElementById("d"+id).style.display = "none";
	document.getElementById("c"+id).style.display = "none";
	document.getElementById("v"+id).style.display = "inline";
	
	
}
function voir(id, groupe)
{
	document.getElementById("d"+id).style.display = "block";
	document.getElementById("v"+id).style.display = "none";
	document.getElementById("c"+id).style.display = "inline";
	
	var quete = "news/webnews/lire.php?article_id=" + encodeURIComponent(id) + "&art_group=" + encodeURIComponent(groupe);
	
	request(quete, readData, id);
}
			

function getXMLHttpRequest() {
    var xhr = null;
    
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest(); 
		}
	} else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    
    return xhr;
}			
	
function request(quete, callback, param) {
	var xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			callback(xhr.responseText, param);
		}
	};
	
	xhr.open("GET", quete, true);
	xhr.send(null);
}

function readData(sData, id) {
	document.getElementById("d"+id).innerHTML = sData;
}
