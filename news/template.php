<?php
/*		
		Auteur : Pierre-Louis GUHUR
		Mail : pierre-louis.guhur@laposte.net
*/



//création des en-têtes et génération du Twig
	
	$module = "Newsgroup";
	$style = BASE.$style;
	$js = BASE."fct.js";

	$entete_replace= array(
	$menunews			=> "class=active",
						);
						
	include(ROOT."templates.php");


//intégration des templates news

	$loader->addPath(TEMPLATE_DIR."news/");
	
//menu des newsgroups

	$test = true;
	foreach($newsgroups_list as $key => $value)  {
		if (strcmp($value, $_SESSION["newsgroup"]) == 0) {
			$groupe = $value;
			$test = false;
		}
	}
	if($test) $groupe = $messages_ini["text"]["newsgroup"]; 
	reset($newsgroups_list);

	$replace = array (
		'groups'		=> $newsgroups_list,
		'groupe'		=> $groupe,
		'navgroup'	 	=> $grp_rub
		);
		
		
	echo $twig->render('news/menu.html', $replace);

	
//page
	
	include($content_page);

?>
