<?php
if(!defined("ROOT"))
      define("ROOT", "../");
$membre = true;
require_once (ROOT.'lib/cas/CAS.php'); //pour se connecter au CAS
 
         $CAS=array(
                 'crans.org' => array( 
                         'nom'           => 'Cr@ns',
                         'host'          => 'cas.crans.org', 
                           'context'       => '/cas', 
                          'port'          => 443, 
                           'root_cert' => '/etc/ssl/certs/cacert.org.pem'
                   ),
                   'ens-cachan.fr' => array( 
                           'nom'           => 'ENS de Cachan',
                           'host'          => 'cas.ens-cachan.fr', 
                           'context'       => '', 
                           'port'          => 443, 
                           'root_cert' => '/etc/ssl/certs/UTN_USERFirst_Hardware_Root_CA.pem'
                   ),
           );
   $cas = "crans.org";
   
   /*initialisation*/
   phpCAS::setDebug();
   phpCAS::client(SAML_VERSION_1_1, $CAS[$cas]['host'], $CAS[$cas]['port'], $CAS[$cas]['context']);
   phpCAS::setNoCasServerValidation();
   phpCAS::setFixedServiceURL("http://bde.crans.org/membre/CAS_Crans_final.php");
   
   /* récupération des info */
   phpCAS::handleLogoutRequests(true, array($CAS[$cas]['host']));
   phpCAS::forceAuthentication();
?>