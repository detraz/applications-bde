<?php
if(!defined("ROOT"))
	define("ROOT", "../");
	

require_once (ROOT.'lib/cas/CAS.php');

	function dbconn(){
		global $dbhost, $dbuser, $dbpasswd, $dbname;

		if (!@mysql_connect($dbhost, $dbuser, $dbpasswd))
		{
		  switch (mysql_errno())
		  {
			case 1040:
			case 2002:
				if ($_SERVER['REQUEST_METHOD'] == "GET")
					die(header('HTTP/1.1 503 Service Unavailable')."<html><head><meta http-equiv=refresh content=\"5 $_SERVER[REQUEST_URI]\"></head><body><table border=0 width=100% height=100%><tr><td><h3 align=center>Le temps de chargement est beaucoup trop haut. Nouvel essai en cours, attendez...</h3></td></tr></table></body></html>");
				else
					die(header('HTTP/1.1 503 Service Unavailable')."Trop d'utilisateurs. Rechargez pour la page pour lancer une nouvelle tentative.");
		default:
			die(header('HTTP/1.1 503 Service Unavailable')."[" . mysql_errno() . "] dbconn: mysql_connect: " . mysql_error());
		  }
		}
		mysql_select_db($dbname)
		or die(header('HTTP/1.1 503 Service Unavailable').'dbconn: mysql_select_db: ' + mysql_error());
	}


	function is_loged(){
		if(isset($_SESSION['auth'])&&$_SESSION['auth']==true){
			return true;
		}else{
			return false;
		}
	}

	function init_cas($cas){
		global $CAS;
		phpCAS::setDebug();
		phpCAS::client(SAML_VERSION_1_1, $CAS[$cas]['host'], $CAS[$cas]['port'], $CAS[$cas]['context']);
		phpCAS::setCasServerCACert($CAS[$cas]['root_cert']);
		phpCAS::handleLogoutRequests(true, array($CAS[$cas]['host']));
	}

	
	function validate_mail($token){
		global $delete_account_after;
		$time=time() - $delete_account_after;
				mysql_query("DELETE FROM ".$table_prefix."users WHERE valid='non' AND inscription<".$time)or die(mysql_error());
		if($token==''){return false;}
		$query=mysql_query("SELECT * FROM ".$table_prefix."users WHERE valid='non' AND url='".mysql_real_escape_string($token)."'");
		if(mysql_num_rows($query)<1){
			return false;
		}else{
			mysql_query("UPDATE ".$table_prefix."users SET valid='oui', url='' WHERE url='".mysql_real_escape_string($token)."'")or die(mysql_error());
			return true;
		}
	}

	function inscription_mail($mail,$token,$nom){
		// Sujet
	 $to  = $mail; // notez la virgule
	 $subject = 'Inscription � l\'application';

	 // message
	 $message = '
	 
Merci de ton inscription sur l\'application BDE de l\'ENS de Cachan.
Pour confirmer votre inscription et pouvoir vous connecter, merci de suivre le lien suivant : 
http'.(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on'?'s':'').'://'.$_SERVER['HTTP_HOST'].'/?confirm='.$token.'


	 ';

	 // Pour envoyer un mail HTML, l'en-t�te Content-type doit �tre d�fini
	 $headers  = 'MIME-Version: 1.0' . "\r\n";
	 $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";

	 // En-t�tes additionnels
	 $headers .= 'To: '.$nom.' <'.$mail.'>' . "\r\n";
	 $headers .= 'From: Web-news<nobody@crans.org>' . "\r\n";

	 // Envoi
	 mail($to, $subject, $message, $headers);
	 }


	function sha1crypt($password){
		// create a salt that ensures crypt creates an sha1 hash
		$base64_alphabet='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
				.'abcdefghijklmnopqrstuvwxyz0123456789+/';
		$salt='$2$';
		for($i=0; $i<9; $i++){
		$salt.=$base64_alphabet[rand(0,63)];
		}
		// return the crypt sha1 password
		return $salt.'$'.base64_encode(sha1($salt.'$'.$password,true));
	}
	//~ echo  sha1crypt('salsa')."\n";
	function validpass($pass,$passhash){
		$salt=substr($passhash,0,13);
		$calc=($salt.sha1($salt.$pass));
		$calc2=($salt.base64_encode(sha1($salt.$pass,true)));
		if($calc==$passhash||$calc2==$passhash){
			return true;
		}else{
			return false;
		}
	}

	function login($mail,$pass,$use_cas=false, &$data = array()){
		global $delete_account_after, $table_prefix;
		if (is_loged()){
			return true;
		}
		if($use_cas){
			init_cas();
			phpCAS::forceAuthentication();
			foreach (phpCAS::getAttributes() as $key => $value) {
				$_SESSION[$key]=$value;
			}
			$where="mail='".phpCAS::getUser()."@".$_SESSION['cas']."' OR mail='".$_SESSION['mail']."'";
			if(array_key_exists('mailAlias', $_SESSION)){
				if (is_array($_SESSION['mailAlias'])) {
					foreach ($_SESSION['mailAlias'] as $mail){
						$where.=" OR mail='".$mail."'";
					}
				}else{
					$where.=" OR mail='".$_SESSION['mailAlias']."'";
				}
			}

		}else{
			$where="nom='".mysql_real_escape_string($mail)."' AND pass='".sha1($pass)."' ";//AND valid='oui'";
		}

		$query=mysql_query("SELECT * FROM ".$table_prefix."users WHERE ".$where." ORDER BY nom")or die(mysql_error());

		if(mysql_num_rows($query)<1){
			if($use_cas&&phpCAS::checkAuthentication()){//premiere connexion sur l'app
				if(empty($_SESSION['cn']))
				{
					$matches = explode('@', $_SESSION['cn']);
					$nom = $matches[0];
				}
				else
					$nom = mysql_real_escape_string($_SESSION['cn']);
					
				  mysql_query("INSERT INTO ".$table_prefix."users (nom,mail,pass,inscription, last_login) VALUES ('".$nom."','".mysql_real_escape_string(phpCAS::getUser()."@".$_SESSION['cas'])."','!','".time()."','".time()."')")or die(mysql_error());  
				  $data=array(
						'nom'  		=> $_SESSION['cn'],
						'mail'		=> $_SESSION['mail']
							);
			}else{
				return false;
			}
		}
		else {
			$data=mysql_fetch_assoc($query);	
		}
	

		return true;

	}

	function logout(){
		$_SESSION['auth']=false;
		if(isset($_SESSION['use_cas'])&&$_SESSION['use_cas']){
			$use_cas=true;
			init_cas();
		}
		foreach($_SESSION as $key => $value){
			unset($_SESSION[$key]);
		}
		if($use_cas){
			 phpCAS::logout();
		}

	}


	function LogoutRequest(){
		if(isset($_POST['logoutRequest'])){
			$cas=implode('.',array_slice(explode('.', gethostbyaddr($_SERVER['REMOTE_ADDR'])),1));
			init_cas(true, $cas);
		}
	}
?>
