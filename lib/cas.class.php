<?php
require_once (ROOT.'lib/cas/CAS.php'); //pour se connecter au CAS

class CASApp {
	var $CAS;
	var $cas;
	
	function CASApp($cas = "") {	
		$this->CAS=array(
			'crans.org' => array( 
				'nom'		=> 'Cr@ns',
				'host' 		=> 'cas.crans.org', 
				'context' 	=> '/cas', 
				'port' 		=> 443, 
				'root_cert' => '/etc/ssl/certs/cacert.org.pem'
			),
			'ens-cachan.fr' => array( 
				'nom'		=> 'ENS de Cachan',
				'host' 		=> 'cas.ens-cachan.fr', 
				'context' 	=> '', 
				'port' 		=> 443, 
				'root_cert' => '/etc/ssl/certs/UTN_USERFirst_Hardware_Root_CA.pem'
			),
		);
	
		/* correctif : destruction toute trace de SESSION ! */
		if(headers_sent()) {
			$error = array(
				"nom" 		=> "headers_sent", 
				"fct"		=> __FUNCTION__,
				"class"		=> __CLASS__
					);
			$this->error("headers_sent");
		}
		session_start();
		$_SESSION = array();
 
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000, $params["path"], ".".$params["domain"],
				$params["secure"], $params["httponly"]
			);
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);			
		}
		session_destroy();
		
		if(!empty($cas)) {
			$this->init($cas);
		}
	}
	function init($cas) {
		$this->cas = $cas;
		/*initialisation*/
		phpCAS::client(SAML_VERSION_1_1, 
				$this->CAS[$cas]['host'], 
				$this->CAS[$cas]['port'], 
				$this->CAS[$cas]['context']);
		phpCAS::setNoCasServerValidation();
		phpCAS::handleLogoutRequests(true, array($this->CAS[$cas]['host']));
	}
	function debug(){
		phpCAS::setDebug();
	}
	function redirection($url){
		phpCAS::setFixedServiceURL($url);
	}
	function connect () {
		phpCAS::forceAuthentication();
	}
	function get_CAS () {
		return $this->CAS;
	}
	function get_CAS_noms () {
		return array_keys($this->CAS);
	}
	function error($error) {
		switch ($error['nom']) {
			case "headers_sent":
				echo "<h1>Erreur : le header a d�j� �t� envoy�</h1><pre>";
				print_r($error);
				echo "</pre>";
				exit;
		}
	}
	function deconnect() {
		phpCAS::logout();
	}
	function get_attributes($redir = false) {
		if($redir)
			phpCAS::isAuthenticated();
		$array = phpCAS::getAttributes();
		$array['nom'] = phpCAS::getUser();
		$array['mail'] = isset($array['mail'])&&filter_var($array['mail'], FILTER_VALIDATE_EMAIL)? $array['mail'] : $array['nom']."@".$cas;
		return $array;
	}
}

?>