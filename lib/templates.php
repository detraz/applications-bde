<?php
/*		
		Auteur : Pierre-Louis GUHUR
		Mail : pierre-louis.guhur@laposte.net
*/
if(!defined("ROOT")) {
	define("ROOT", "./");
}

require_once(ROOT."config.php");

/// le fichier permet de g�n�rer l'objet Twig indispensable aux templates
/// il affiche �galement l'en-t�te



//choix du template
$templates = array ("defaut");//contient le nom des dossiers des diff�rents templates possibles

if (!defined("TEMPLATE"))
{
	define("TEMPLATE", "defaut");
	define("TEMPLATE_DIR", ROOT."templates/defaut/");
}

if(!in_array(TEMPLATE, $templates))
	exit("Le template s�lectionn� n'existe pas");

//g�n�ration de l'objet Twig
require_once (ROOT.'lib/Twig/Autoloader.php');
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(array(ROOT.'templates/'.TEMPLATE));
$twig = new Twig_Environment($loader, array(
	'cache' => false
));//le cache est d�sactiv�


//affichage de la barre d'en-t�te
	if(!isset($entete_replace))
		$entete_replace = array();
		
	include(TEMPLATE_DIR."config.php");//valeur par d�faut de $entete_replace
	
	$entete_replace = array_merge(
		array(
			'nom' 			=> $nom_app,
			'module' 		=> $module,
			'style'			=> $style,
			'style_tpl'		=> $style_tpl,
			'js'			=> $js,
			'l'				=> $langue->out(),
			'root'			=> ROOT

						),
		$entete_replace);
	
	echo $twig->render('head.app.html', $entete_replace);
	echo $twig->render('menu.app.html', $entete_replace);
	

?>