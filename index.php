<?php

if(!defined("ROOT"))
	define("ROOT", "./");
	
	require_once(ROOT."config.php");
	require_once(ROOT."lib/log.php");
	require_once(ROOT."lib/utile.php");

	
//gérer la déconnexion à l'application
	if(is_requested('logout')){
		logout();
        header("Location: ".construct_url($logout_url));
		exit;
	}

	
//afficher l'en-tête
	$entete_replace = array(
	'module' 		=> "Accueil",
	'nom' 			=> "BDE ENS Cachan",
	'menuhome'		=> "class=active",
	'l'				=> $messages_ini,
	'root'			=> "./"
						);
	include("templates.php");
	
	
	
?>
<div class="panel panel-primary">
	<div class="panel-heading">Bienvenue sur l'application web des [list]ériks !</div>
	<div class="panel-body">
Parce que les [list]ériks sont toujours à portée de main,
<br />Parce que les [list]ériks savent innover,
<br />Parce que les [list]ériks vous proposent un calendrier bien chargé,
<br />Parce que les [list]ériks aiment bien les news...
<h4>Nous vous proposons l'application web du BDE !</h4>

<div class="alert alert-warning">
<span class="glyphicon glyphicon-warning-sign"></span> 
L'application mobile est en cours de développement !
<br />N'hésitez pas à faire vos remarques au <a href="mailto:pierre-louis.guhur@ens-cachan.fr">webmestre</a>.
<br />L'affichage est optimisée pour les petits écrans.
</div>
	</div>
</div>