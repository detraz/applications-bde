{HEAD}
{HEADER}
{MENU}
<table border="0" cellspacing="0" cellpadding="0" class="maintable">
	<tr>
		<td valign="top">
		
			<table  width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr class="allday">
					<td>
						<!-- loop allday on -->
						<div class="alldaybg_{CALNO}">
							{ALLDAY}
						</div>
						<!-- loop allday off -->
					</td>
				</tr>
      			<tr>
					<td align="center" valign="top" colspan="3">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tfixed">
							<!-- loop row on -->
							<tr>
								<td rowspan="4" align="center" valign="top" width="60" class="timeborder">9:00 AM</td>
								<td width="1" height="15"></td>
								<td class="dayborder">&nbsp;</td>
							</tr>
							<tr>
								<td width="1" height="15"></td>
								<td class="dayborder2">&nbsp;</td>
							</tr>
							<tr>
								<td width="1" height="15"></td>
								<td class="dayborder">&nbsp;</td>
							</tr>
							<tr>
								<td width="1" height="15"></td>
								<td class="dayborder2">&nbsp;</td>
							</tr>
							<!-- loop row off -->
							<!-- loop event on -->
							<div class="eventfont">
								<div class="eventbg_{EVENT_CALNO}">{CONFIRMED}<b>{EVENT_START}</b> - {EVENT_END}</div>
								<div class="padd">{EVENT}</div>
							</div>
							<!-- loop event off -->
						</table>
					</td>
				</tr>
        	</table>
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="tbll"><img src="images/spacer.gif" alt="" width="8" height="4" /></td>
					<td class="tblbot"><img src="images/spacer.gif" alt="" width="8" height="4" /></td>
					<td class="tblr"><img src="images/spacer.gif" alt="" width="8" height="4" /></td>
				</tr>
			</table>
    	</td>
	</tr>
</table>
</center>
{FOOTER}

