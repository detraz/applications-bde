<div class="row medtitle">
	{MONTH_TITLE}
</div>
<div class="row">
	<!-- loop weekday on -->
	<div class="yearweek col-xs-7">{LOOP_WEEKDAY}</div>
	<!-- loop weekday off -->
</div>

<!-- loop monthweeks on -->
	<div class="row monthweeks">
	<!-- loop monthdays on -->
	
		<!-- switch notthismonth on -->
		<div class="yearoff col-xs-7">
			<a class="psf" href="day.php?cal={CAL}&amp;getdate={DAYLINK}">{DAY}</a>
		</div>
		<!-- switch notthismonth off -->
		<!-- switch istoday on -->
		<div class="yearon  col-xs-7">
			<a class="psf" href="day.php?cal={CAL}&amp;getdate={DAYLINK}">{DAY}</a>
			<div align="center">
				<a class="psf" href="day.php?cal={CAL}&amp;getdate={DAYLINK}">{ALLDAY}</a>
				<a class="psf" href="day.php?cal={CAL}&amp;getdate={DAYLINK}">{EVENT}</a>
			</div>
		</div>
		<!-- switch istoday off -->
		<!-- switch ismonth on -->
		<div class="yearreg  col-xs-7">
			<a class="psf" href="day.php?cal={CAL}&amp;getdate={DAYLINK}">{DAY}</a>
			<div align="center">
				<a class="psf" href="day.php?cal={CAL}&amp;getdate={DAYLINK}">{ALLDAY}</a>
				<a class="psf" href="day.php?cal={CAL}&amp;getdate={DAYLINK}">{EVENT}</a>
			</div>
		</div>
		<!-- switch ismonth off -->
		<!-- loop monthdays off -->
	</div>
	<!-- loop monthweeks off -->	
	