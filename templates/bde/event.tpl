<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>BDE ENS Cachan</title>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.3.0/pure-min.css"></link>
	<link rel="stylesheet" type="text/css" href="{DEFAULT_PATH}/templates/{TEMPLATE}/default.css" />
	<script id="nicetitle" type="text/javascript" src="{DEFAULT_PATH}/nicetitle/nicetitle.js"></script>
	<link rel="stylesheet" type="text/css" href="{DEFAULT_PATH}/nicetitle/nicetitle.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- switch rss_available on -->
	<link rel="alternate" type="application/rss+xml" title="RSS" href="{DEFAULT_PATH}/rss/rss.php?cal={CAL}&amp;rssview={CURRENT_VIEW}">
	<!-- switch rss_available off -->		


</head>
<body>

<div class="panel panel-primary">
	<div class="panel-heading">
				{CAL_TITLE_FULL}
	</div>
	<div class="panel-body">
	<table class="table">
		<tr>
			<th>{L_SUMMARY}</th>
			<td>{EVENT_TEXT} - <span class="V9">(<i>{EVENT_TIMES}</i>)</span></td>
		</tr>
		<!-- switch description on -->
		<tr><th>{L_DESCRIPTION}</th><td> {DESCRIPTION}</td>
		<!-- switch description off -->
				
		<!-- switch organizer on -->
		<tr><th>{L_ORGANIZER}</th><td>{ORGANIZER}</td></tr
		<!-- switch organizer off -->
		<!-- switch attendee on -->
		<tr><th>{L_ATTENDEE}</th><td>{ATTENDEE}</td></th>
		<!-- switch attendee off -->
		<!-- switch status on -->
		<tr><th>{L_STATUS}</th><td>{STATUS}</td></tr>
		<!-- switch status off -->
		<!-- switch location on -->
		<tr><th>{L_LOCATION}</th><td>{LOCATION}</td>
		<!-- switch location off -->
		<!-- switch url on -->
		<tr><th>{L_URL}</th><td>{URL}</td></tr>
	</table>
		<!-- switch url off -->
		<!-- switch event download on -->
		{EVENT_DOWNLOAD}<br />
		<!-- switch event download off -->
					</p>
</div>
</body>
</html>

