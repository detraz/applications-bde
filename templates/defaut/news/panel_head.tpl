


<div class="row">
<div class="btn btn-group" >
	<button type="button" class="btn btn-default">
		<a href="?renew=0&sort=0">{{ lang.text.subject }}</a>
		{% if sort_by == 0 %}
			<img src="{{ arrow_img }}" />
		{% endif %}

	</button>
	<button type="button" class="btn btn-default">
		
		<a href="?renew=0&sort=1">{{ lang.text.sender }}</a>
		{% if sort_by == 1 %}
			<img src="{{ arrow_img }}" />
		{% endif %}

	</button>
	<button type="button" class="btn btn-default">
		
		<a href="?renew=0&sort=2">{{ lang.text.date }}</a>
		{% if sort_by == 2 %}
			<img src="{{ arrow_img }}" />
		{% endif %}

	</button>
</div>
</div>

<div class="row">
{% if pagination == true %}
	{% include 'pagination.html' %}
{% endif %}
</div>