<div class="panel panel-primary">
	<div class="panel-heading">
		{{ l.membre.info }}
	</div>
	<div class="panel-body">
		<form method="post" action="">
		<table class="table">
			<tr>
				<td>{{ l.connect.pseudo }}</td>
				<td>{{ pseudo }}</td>
			</tr>
			<tr>
				<td><label for="mail">{{ l.membre.mail }}</label></td>
				<td><input type="text" name="mail" value="{{ mail }}" id="mail"/></td>
			</tr>
			<tr>
				<td><label for="tel">{{ l.membre.tel }}</label></td>
				<td><input type="text" name="tel" value="{{ tel }}" id="tel"/></td>
			</tr>
			<tr>
				<td><label for="where">{{ l.hotline.where }}</label></td>
				<td><input type="text" name="where" value="{{ bed }}" id="where"/></td>
			</tr>						
			<tr>
				<td><label for="when">{{ l.hotline.when }}</label></td>
				<td><input type="text" name="when" value="{{ when }}" id="when"/></td>
			</tr>			
			<tr>
				<td><label for="ask">{{ l.hotline.ask }}</label></td>
				<td><textarea name="ask"></textarea></td>
			</tr>		
		</table>
		
		<input type="submit" name="submit" />
		</form>
		
	</div>
</div>