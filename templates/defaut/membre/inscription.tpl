<div class="panel panel-primary">
	<div class="panel-heading">
		{{ l.membre.inscription }}
	</div>
	<div class="panel-body">
		<form method="post" class="pure-form" action="{{ root }}membre/inscription.php">
		<fieldset class="pure-group">
			<input type="text" name="name" value="{{ name }}" id="name" class="pure-input-1" placeholder="{{ l.connect.pseudo}}" />
			<input type="password" name="pw" value="{{ pw }}" id="pw" class="pure-input-1" placeholder="{{ l.connect.pw}}" />
		</fieldset>
		<fieldset class="pure-group">
			<input type="text" name="tel" value="{{ tel }}" class="pure-input-1" placeholder="{{ l.membre.tel }}"  id="tel"/>
			<input type="text" name="mail" value="{{ mail }}" id="mail" class="pure-input-1" placeholder="{{ l.membre.mail}}" />
			<input type="text" value="{{ chambre }}" id="chambre" name="chambre" class="pure-input-1" placeholder="{{ l.membre.bed}}" />
		</fieldset>
		<button type="submit" class="pure-button pure-input-1 pure-button-primary" name="submit">S'inscrire</button>
		</form>
	</div>
</div>