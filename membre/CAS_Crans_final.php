<?php
   
   if(!defined("ROOT"))
           define("ROOT", "../");
           
   $membre = true;
   
   require_once (ROOT.'lib/cas/CAS.php'); //pour se connecter au CAS
   
  /* traitement */
   
   require_once(ROOT.'config.php');        //pour se connecter à la BDD
        $CAS=array(
                 'crans.org' => array( 
                         'nom'           => 'Cr@ns',
                         'host'          => 'cas.crans.org', 
                           'context'       => '/cas', 
                          'port'          => 443, 
                           'root_cert' => '/etc/ssl/certs/cacert.org.pem'
                   ),
                   'ens-cachan.fr' => array( 
                           'nom'           => 'ENS de Cachan',
                           'host'          => 'cas.ens-cachan.fr', 
                           'context'       => '', 
                           'port'          => 443, 
                           'root_cert' => '/etc/ssl/certs/UTN_USERFirst_Hardware_Root_CA.pem'
                   ),
           );
 $cas = "crans.org";
 
   /*initialisation*/
   phpCAS::setDebug();
   phpCAS::client(SAML_VERSION_1_1, $CAS[$cas]['host'], $CAS[$cas]['port'], $CAS[$cas]['context']);
   phpCAS::setNoCasServerValidation();
   if(!phpCAS::isAuthenticated())
	{ 
		$error = "cas_failed";
		include(ROOT."membre/connect.php");
		exit;
	}
   
   
   foreach (phpCAS::getAttributes() as $key => $value) {
           $_SESSION[$key]=$value;
   }
   $mail = isset($_SESSION['mail'])&&filter_var($_SESSION['mail'], FILTER_VALIDATE_EMAIL)? $_SESSION['mail'] : phpCAS::getUser()."@crans.org";
   
   //est-ce que l'email est déjà enregistré ?
   $query=mysql_query("SELECT * FROM ".$table_prefix."users WHERE mail = '".mysql_real_escape_string($mail)."' ORDER BY nom")or die(mysql_error());
   if(mysql_num_rows($query)>=1){
           $data=mysql_fetch_assoc($query);
           $_SESSION['auth']=true;
           $_SESSION['pseudo']=htmlspecialchars(stripslashes($data['nom']));
           $_SESSION['user_id']=filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
           $_SESSION['mail']=htmlspecialchars(stripslashes($data['mail']));
           $_SESSION['chambre'] = htmlspecialchars(stripslashes($data['chambre']));
           $_SESSION['tel'] = filter_var($data['tel'], FILTER_SANITIZE_NUMBER_INT);
          $_SESSION['news'] = htmlspecialchars(stripslashes($data['news']));
   }
   //autrement, on l'enregistre
   else {
           mysql_query("INSERT ".$table_prefix."users 
                                   (nom, mail, news)
                                   VALUES ('".mysql_real_escape_string(phpCAS::getUser())."', '".mysql_real_escape_string($mail)."', '".mysql_real_escape_string(phpCAS::getUser())."')
                                   ")or die(mysql_error());
           $_SESSION['auth']=true;
           $_SESSION['pseudo']=htmlspecialchars(stripslashes($data['nom']));
           $_SESSION['user_id']=filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
           $_SESSION['mail']=htmlspecialchars(stripslashes($data['mail']));
           $_SESSION['chambre'] = htmlspecialchars(stripslashes($data['chambre']));
           $_SESSION['tel'] = filter_var($data['tel'], FILTER_SANITIZE_NUMBER_INT);
           $_SESSION['news'] = htmlspecialchars(stripslashes($data['news']));
           
                                   }
   
   header("Location: ".ROOT."membre/index.php?co=1");
  echo '<a href="'.ROOT.'">Page d\'accueil</a>';
   ?>