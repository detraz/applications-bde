<?php
if(!defined("ROOT"))
	define("ROOT", "../");
	
include(ROOT."config.php");

if(isset($_POST['mail']))
	$mail = htmlspecialchars($_POST['mail']);
else
	$mail = $_SESSION['mail'];
	
if(isset($_POST['where']))
	$lieu = htmlspecialchars($_POST['where']);
else
	$lieu = $_SESSION['chambre'];
	
if(isset($_POST['tel']))
	$tel = filter_var($_POST['tel'], FILTER_SANITIZE_NUMBER_INT);
else
	$tel = $_SESSION['tel'];

	
//si on a cherché à poster sur la hotline
if(isset($_POST['submit'])) {
	if(!empty($_POST['mail']) && !empty($tel))
	{
		mysql_query("INSERT INTO ".$table_prefix."hotline 
		(`user_id`, `mail`, 
		`when`, `where`, `ask`, `tel`)
		VALUES ('".$_SESSION['user_id']."',
					'".$mail."',
					'".mysql_real_escape_string($_POST['when'])."',
					'".$lieu."',
					'".mysql_real_escape_string($_POST['ask'])."',
					'".$tel."'
					) ")or die(mysql_error()); 
					
		$message = "
			Hello [List]érikien !\n
			\n
			On a une nouvelle commande à servir !
			
			De : ".htmlspecialchars($_SESSION['pseudo'])."\n
			Où : ".htmlspecialchars($_POST['where'])."\n
			Quand : ".htmlspecialchars($_POST['when'])."\n
			Quoi : ".htmlspecialchars($_POST['ask'])."\n
			\n
			Pour le contacter :\n
			\n
			Mail : ".htmlspecialchars($_POST['mail'])."\n
			Tél. : ".htmlspecialchars($_POST['tel'])."\n
			\n\n
			--
			L'application mobile";
		mail("hotline.listerik@gmail.com", "[Commande] Date : ".htmlspecialchars($_POST['when'])."  Lieu : ".htmlspecialchars($_POST['where']), $message);
		$success = "hotline";
	}
	else
	{
		$error = "fake_donnee";
	}
	
}

$entete_replace= array(
	"menuhot"			=> "class=active",
						);
include_once(ROOT."templates.php");
include(ROOT."lib/success.php");
include(ROOT."lib/error.php");
$replace = array(
	"pseudo"	=> $_SESSION['pseudo'],
	"mail"		=> $mail,
	"bed"		=> $lieu,
	"tel"		=> $tel,
	"when"		=> htmlspecialchars($_POST['when']),
	"where"		=> htmlspecialchars($_POST['where']),
	"root"		=> ROOT,
	"l"			=> $langue->out()
			);

echo $twig->render("hotline/send.html", $replace);


?>